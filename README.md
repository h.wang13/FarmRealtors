FarmRealtors README.md
# Farm Realtors
FarmRealtors is a project focused on creating a platform to sell and buy plots of land for rural areas. It's main selling point is the map that shows the location of the plots of land in a 2D grid giving the buyer a sense of where they would be.

# Usage
Since admin has access to all functions login as admin (email: edgyboi@gmail.com, password: 2edgy4me, remember to check admin box)

Once logged in, you will be redirected to the Main Screen. From here you can choose from 5 buttons to explore the program. The first button will redirect you to the map, click this first. You will find yourself viewing a grid and the available properties on it. If you click on a property that is not sold, you will be redirected to the house details page. Press the home button to return to the Main Screen. 

Next is the "find a home" button, once pressed it will take you to a screen where you can input the price and style (which you can find from the map). It will find a property on the map with those details and pull up the house details. Press Go Home to return to the main screen. 

After that is buy property, which takes you to a screen where you input the price, style, size, age, and realtor. Once "Buy" is pressed the property will be assigned to the customer. 

If you are an admin or realtor, the addProperty button is next. If you press it you will be redirected to a screen where you can input the details necessary to create a house. Press "Create!" to make a new property (needs to be implemented). Press home to go to the home screen.

If you are an admin, you will have access to the createCustomer button. Once pressed you will be taken to a screen where you can input the details needed to create a Customer. Press "Create!" to make a new customer under User (needs to be implemented). Press Go Home to return to the main screen.

Press the logout button in the top-right corner to exit the program.


# Documentation
### User class
The user class is an abstract class with the basic functions that all user classes would have. The constructor takes in the email, age, password, and username in that order.

Authenticate takes in the email and passwords and returns true or false depending on if the credentials are valid.

WriteToFile is used to create the string for the specific user in the text file storing the users, because password is private and should not be accessed from other places. 
#### Customer
Customer inherits from the user class, and only has one additional field being the double creditCardBalance. In addition it has another buyProperty function that calls a function from realtor class to approve the purchase. 
#### Realtor
Inherits from user class and can add and remove homes as well as approve purchases. Has an additional field being the ID. 
#### Admin
Not programmed in due to the lack of time, as well as confusing use since it would be run from the console rather than explicitely using GUI which was found to be unintuitive. 
### Main GUI
This is the mainGUI that redirects to MapGUI, BuyPropertyGUI, AddPropertyGUI, createCustomerGUI, and FindHomeGUI. It allows the user to carry out various functions of the program. AddPropertyGUI is only visible if user is a Realtor or Admin, and createCustomerGUI is only visible if user is a Admin.

### Map GUI
The map is a JTable that has it's data filled from a 2D array in the main class. It uses the HomeRenderer class to render each individual cell. 

#### HomeRenderer
extends JScrollPane and implements TableCellRenderer. By using a JTextArea inside of the cell it is able to create a larger area for text. Then by using color it is able to show different colors depending if the home is sold or not. To actually show the text it uses a method known as SetValue in order to render the cell's contents, and takes in an Object that is later casted into Home, or depending on the situation shows an error. 
### Load files 
Loading files for both would use a nested list due to the two dimensional nature of the storage
#### Users
From the text file determining which section is customers, realtors, and admins the method would then create a map, mapping the email to a User class. Then using a lambda function the map of maps of users is instantiated in the main class


#### Homes
A text file with sections indicating each row is loaded in, and position is automatically filled into the constructors. Then using an indicator phrase("new row") it would create one row, being an arraylist, and add it's contents to the bigger arraylist. Resulting in a 2D arraylist that is then casted into a 2D array with null padding for the JTable map.

### Save Files
Save files both would use nested loops in order to update the text files.
#### Users
For users due to the fact that a map of maps is used, a nested lambda function adds the user details from each user class(realtor, customer, admin), and automatically adds in the correct flags to indicate the list at the end. 
#### Homes
For saving homes a simple nested for loop is used to add in each row to the text files.

# Broken: 
* Buy property
* Create customer
* Admin functions
* Remove property
* Call support
* Update house details
* Ban User
* Add realtor account
