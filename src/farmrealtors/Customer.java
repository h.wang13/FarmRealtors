
package farmrealtors;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author m.pal1
 */
public class Customer extends User{
    
    protected String lookingFor;
    private int creditCard; 
    double creditCardBalance;
    
    public Customer(String e, int a, String p, String u, double cCB){
        super(e,a,p,u);
        creditCardBalance = cCB;
    }
    
    public String getStyle(){
        return "";
    }
    
    public void buyProperty(int y, int x){
    //Realtor.approvePurchase(int y, int x, creditCardBalance);
        if(FarmRealtors.readHomes.get(y).get(x).seller.approvePurchase(y, x, creditCardBalance)){
            FarmRealtors.homes[y][x].sell();
            FarmRealtors.readHomes.get(y).get(x).sell();
        }
    }
    
    @Override
    public String writeToFile(){
        /*try{
            PrintWriter file= new PrintWriter(new FileWriter(filename, true));
            file.println(email + ", "+ age + ", " + password + ", " + username + ", " + creditCardBalance);
        } catch(IOException ex){
            System.out.println(ex.toString());
        }*/
        return (email + ", "+ age + ", " + password + ", " + username + ", " + creditCardBalance);
    }
}
