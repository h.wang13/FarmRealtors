/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package farmrealtors;

import java.awt.Point;
import java.util.Scanner;

/**
 *
 * @author h.wang13
 */
public class Home extends Object{
    Scanner numscan = new Scanner(System.in);
    Scanner wordscan = new Scanner(System.in); 
    
    public Point location;
    public double price;
    public double size;
    public int age;
    public String style;
    public Realtor seller;
    private boolean sold;
    
   
    
    public Home(int x, int y, double p, double s, int a, String st, String r){
        location = new Point(x,y);
        price = p;
        style = st;
        size = s;
        age = a;
        sold = false;
        //Thus assuming that the users will be loaded befoer the homes
        //as well as r being the realtor email
        seller = Realtor.class.cast(FarmRealtors.users.get("realtors").get(r));
    }
    
    public boolean getSold(){
        return sold;
    }
    
    public void sell(){
            sold = true;

    }
    
    public Realtor getSeller(){
        return seller;
    }
}
