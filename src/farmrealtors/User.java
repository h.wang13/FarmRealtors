/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package farmrealtors;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


public abstract class User {
    
    String email;
    int age;
    protected String password;
    String username;
    
    
    public User(String e, int a, String p, String u){
        email = e;
        age = a;
        password = p;
        username = u;
    }
    
    public boolean authenticate(String e, String p){
        if(email.equals(e) && password.equals(p))
            return true;
        return false;
    }
    
    public String writeToFile(){
        //try{
        //    FileWriter file= new FileWriter(filename, true);
        //    file.write(email + ", "+ age + ", " + password + ", " + username );
        //} catch(IOException ex){
        //    System.out.println(ex.toString());
        //}
        return (email + ", "+age+", "+password+", "+username);
    }
    
}
