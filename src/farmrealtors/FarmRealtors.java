/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package farmrealtors;

/**
 *
 * @author h.wang13
 */

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
public class FarmRealtors {
    /**
     * @param args the command line arguments
     */    
    //hashmap of hashmap for user groups + users
    static Map<String, Map<String, User>> users = new HashMap(); 
    static User loggedIn = null;
    //FarmRealtors.users.get("customers").get(email).authenticate(email, password)
    static ArrayList<ArrayList<Home>> readHomes = new ArrayList();
    static Home[][] homes;
    static String[] headers;
    public static void main(String[] args) {
        // TODO code application logic here
        //load users first for realtors, and so that the homes can have their sellers
        loadUsers("users.txt");
        //saveUsers("users.text", users);
        users.get("customers").forEach((k,v) -> {
           System.out.println(k); 
           System.out.println(v.password);
        });
        //int rowLength = loadHomes("homes.txt");
        //saveHome("homes.text", readHomes);
        updateHomes(loadHomes("homes.txt"));
        /*homes = new Home[readHomes.size()][];
        for (int i = 0; i < readHomes.size(); i++) {
            ArrayList<Home> row = readHomes.get(i);
            homes[i] = row.toArray(new Home[rowLength]);
        }
        ArrayList<String> titles = new ArrayList();
        for(int i = 0; i < rowLength; i++){
            titles.add(Integer.toString(i));
        }
        headers = titles.toArray(new String[titles.size()]);*/
        
        new LoginGui().setVisible(true);
        //new MainGui().setVisible(true);
    }
    
    public static void updateHomes(int length){
        homes = new Home[readHomes.size()][];
        for (int i = 0; i < readHomes.size(); i++) {
            ArrayList<Home> row = readHomes.get(i);
            homes[i] = row.toArray(new Home[length]);
        }
        ArrayList<String> titles = new ArrayList();
        for(int i = 0; i < length; i++){
            titles.add(Integer.toString(i));
        }
        headers = titles.toArray(new String[titles.size()]);
    }
    
    
    public static int loadHomes(String filename){//, ArrayList tempList ) {
        String temp;
        int rowCounter = 0;
        int columnCounter = 0;
        int longestRow = -1;
        ArrayList<Home> row = new ArrayList();
        //ArrayList<ArrayList<Home>> readHomes = new ArrayList();
        try {
            BufferedReader file = new BufferedReader(new FileReader(filename));
            while (file.ready()) {
                temp = file.readLine();
                if(temp.equals("new row")){
                    
                    rowCounter++;
                    
                    if(columnCounter > longestRow)
                        longestRow = columnCounter;
                    
                    ArrayList<Home> temporary = new ArrayList();
                    
                    for(Home h: row)
                        temporary.add(h);
                    columnCounter = 0;
                    readHomes.add(temporary);
                    row.clear();
                } else {
                String tempArray[] = temp.split(",");
                
                row.add(new Home(columnCounter, rowCounter, Double.parseDouble(tempArray[0]), Double.parseDouble(tempArray[1]), Integer.parseInt(tempArray[2].trim()), tempArray[3].trim(), tempArray[4].trim()));
                columnCounter++;
                }
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        return longestRow;
    }//end loadHomes

    //For reading in the loadusers it is always email, age, password, username
    //exception being realtor with an ID at the very end
    
    public static void loadUsers(String filename){
        String temp = "";
        Map<String, User> userStore = new HashMap();
        int mode = 0;//pointer to either admin(0), realtor(1), or customer(2)
        try {
        BufferedReader file = new BufferedReader(new FileReader(filename));
            while (file.ready()) {
                temp = file.readLine();
                String tempArray[] = temp.split(","); 
                if(tempArray[0].substring(0,2).equals("//")){
                    if(tempArray[0].equals("//admins")){
                        mode = 1;
                        Map<String, User> temporary = new HashMap();//userStore;
                        userStore.forEach((k,v)->{
                            temporary.put(k, v);
                        });
                        users.put("admins", temporary);
                    
                        userStore.clear();
                    } else if(tempArray[0].equals("//realtors")){
                        mode = 2;
                        Map<String, User> temporary = new HashMap();
                        userStore.forEach((k,v)->{
                            temporary.put(k, v);
                        });
                        users.put("realtors", temporary);
                    
                        userStore.clear();
                    } else if(tempArray[0].equals("//customers")){
                        //mode = 3;
                        Map<String, User> temporary = new HashMap();
                        userStore.forEach((k,v)->{
                            temporary.put(k, v);
                        });
                        users.put("customers", temporary);
                    
                        userStore.clear();
                        break;
                    }
                } else {
                    if(mode == 0){
                        userStore.put(tempArray[0].trim(), new Admin(tempArray[0].trim(), Integer.parseInt(tempArray[1].trim()), tempArray[2].trim(), tempArray[3].trim()));
                    } else if(mode == 1){
                        userStore.put(tempArray[0].trim(), new Realtor(tempArray[0].trim(), Integer.parseInt(tempArray[1].trim()), tempArray[2].trim(), tempArray[3].trim(), Integer.parseInt(tempArray[4].trim())));
                    } else if(mode == 2){
                        userStore.put(tempArray[0].trim(), new Customer(tempArray[0].trim(), Integer.parseInt(tempArray[1].trim()), tempArray[2].trim(), tempArray[3], Double.parseDouble(tempArray[4])));
                    }
                }
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }
    
    public static void saveHome(String filename, ArrayList<ArrayList<Home>> tempList){
        try {
            PrintWriter file = new PrintWriter(new FileWriter(filename));

            for (int i = 0; i < tempList.size(); i++) {
                ArrayList<Home> row = tempList.get(i);
                for(int n=0; n<row.size(); n++){
                    String toSave = Double.toString(row.get(n).price);
                    toSave += ", " + row.get(n).size;
                    toSave+= ", " + row.get(n).age;
                    toSave+=", " + row.get(n).style;
                    toSave+=", " + row.get(n).seller.email;
                    file.println(toSave);
                }
                file.println("new row");
            }
            file.close();
            } catch (IOException ex) {
                System.out.println(ex.toString());
            }
    }
    
    public static void saveUsers(String filename, Map<String, Map<String, User>> tempMap ) {
        try {
            PrintWriter file = new PrintWriter(new FileWriter(filename));
            tempMap.forEach((oK, oV) -> {
            oV.forEach((_k, v) -> {
                file.println(v.writeToFile());
            });
            file.println("//"+oK);
            });
            file.close();
        } catch (IOException ex) {
            Logger.getLogger(FarmRealtors.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


}
